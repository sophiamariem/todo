//
//  AddToDoItemViewController.h
//  Example
//
//  Created by Sophia Mina on 05/06/2014.
//  Copyright (c) 2014 Example. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "exampleToDoItem.h"

@interface AddToDoItemViewController : UIViewController

@property exampleToDoItem *toDoItem;

@end
