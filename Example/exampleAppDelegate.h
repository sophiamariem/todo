//
//  exampleAppDelegate.h
//  Example
//
//  Created by Sophia Mina on 04/06/2014.
//  Copyright (c) 2014 Example. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface exampleAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
