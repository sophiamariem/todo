//
//  exampleToDoItem.h
//  Example
//
//  Created by Sophia Mina on 06/06/2014.
//  Copyright (c) 2014 Example. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface exampleToDoItem : NSObject

@property NSString *itemName;
@property BOOL completed;
@property (readonly) NSDate *creationDate;

@end
