//
//  main.m
//  Example
//
//  Created by Sophia Mina on 04/06/2014.
//  Copyright (c) 2014 Example. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "exampleAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([exampleAppDelegate class]));
    }
}
